<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimal-ui" />
    <title>Sweepstakes</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/android-chrome-manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script>
      // touchSupport
      touchSupport = (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
      if (!touchSupport) {
        document.documentElement.className += " no-touch";
      } else {
        document.documentElement.className += " touch";
      }
       // IE version
      var uA = window.navigator.userAgent;
      var msie = uA.indexOf('MSIE ');
      var trident = uA.indexOf('Trident/');
      var ie;
      function detectIE() {
        if (msie > 0) {
          // IE 10 or older => return version number
          ie = 'ie' + parseInt(uA.substring(msie + 5, uA.indexOf('.', msie)), 10);
          return ie;
        }
        if (trident > 0) {
          // IE 11 (or newer) => return version number
          var rv = uA.indexOf('rv:');
          ie = 'ie' + parseInt(uA.substring(rv + 3, uA.indexOf('.', rv)), 10);
          return ie;
        }
        return ie = 'nonIE';
      }
      detectIE();
      if (ie === 'nonIE') {
        document.documentElement.className += (' ' + ie);
      } else {
        document.documentElement.className += (' ' + (ie + ' ' + 'IE'));
      };
       // Google Analytics tracking code
      (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-410969-1', 'auto');
      ga('send', 'pageview');
    </script>
    
    
    <link rel="stylesheet" href="main.css">
  </head>
  <body>

    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong>  browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>  to improve your experience.</p>
            <![endif]-->
    <header class="visit-sweden-header">
      <nav class="navbar navbar-default  visit-sweden-main-nav-root-btn" role="navigation">
        <div class="container">
          <a class="visit-sweden-logo" href="index.php" title="To startpage">
            <img src="images/Visit_Sweden_logotype-negative2.svg" alt="logo">
            <p>In a
              <br>Volvo</p>
          </a> 
          <div class="navbar-header">
            <button type="button" class="navbar-toggle visit-sweden-nav-toggle-button collapsed" data-toggle="collapse" data-target="#visit-sweden-root-main-navbar-collapse">
              <span class="sr-only">Toggle navigation</span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="visit-sweden-root-main-navbar-collapse">
            <div id="visit-sweden-nav-search-collapse" class="visit-sweden-main-search-collapse collapse">
              <div class="container">
                <form class="search" role="search" action="search.html">
                  <span class="icons icon-t3-search"></span> 
                  <div class="form-group" id="the-basics">
                    <input type="search" class="form-control typeahead quicksearch" placeholder="Search">
                    <button type="submit" style="display:none"></button>
                  </div>
                </form>
              </div>
            </div>

            <?php include 'inc-menu.php';?>
            <!-- <ul class="nav navbar-nav">
              <li> <a href="index.php">Home</a>  </li>
              <li class="active">
                <a href="contest.html">Contest</a> 
              </li>
              <li> <a href="travellers.html">Travellers</a>  </li>
              <li> <a href="activities.html">Activities</a>  </li>
            </ul> -->
          </div>
        </div>
      </nav>
      <div id="pxa-search-collapse" class="pxa-main-search-collapse collapse ">
        <div class="container">
          <form class="search" role="search" action="search.html">
            <span class="icons icon-t3-search"></span> 
            <div class="form-group" id="the-basics">
              <input type="search" class="form-control typeahead quicksearch" placeholder="Search">
              <button type="submit" style="display:none"></button>
            </div>
          </form>
        </div>
      </div>
    </header>
    <div class="visit-sweden-page100">
      <div class="visit-sweden-header-block" style="background-image: url(images/header-beach.jpg);">
        <div class="visit-sweden-header-block-content visit-sweden-logo-top visit-sweden-white-border">
          <div class="visit-sweden-logo-link visit-sweden-logo-left visit-sweden-logo-white-background">
            <a href="index.php">
              <img src="images/Visit_Sweden_logotype.svg" alt="" />
            </a> 
          </div>
          <div class="visit-sweden-header-block-description">
            <p>Sweepstakes</p>
            <h1>Win a trip
              <br />to Sweden</h1>
          </div>
        </div>
      </div>
      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <h3 class="text-center">Win a trip to Sweden</h3>
              <h2 class="text-center">Sweepstakes</h2>
              <p class="text-center">The Sweepstakes has ended. The winner will be contacted directly. Good luck to everyone who participated.</p>
              <!-- <p class="text-center">
                Enter the sweepstakes for the chance to fly Scandinavian airlines round-trip to the city of Gothenburg - the hometown of Volvo and the Volvo factory - for free!  It just may be your chance to experience first-hand the adventures of our favorite bloggers.
              </p>
              <div class="row">
                <div class="col-md-6">
                  <div class="image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/competition-image.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-md-6">
                  <form class="form-horizontal visit-sweden-contest-form" action="sendEmail.php" id="visitsweden-contest-form" name="visit-sweden-ny-contest" data-parsley-validate>
                    <div class="form-group">
                      <div class="col-md-12">
                        <input class="form-control input-field-text" type="text" value="" name="name" placeholder="* Full name" required="true" data-parsley-required="true">
                      </div>
                      <div class="col-md-12">
                        <input class="form-control input-field-text" type="email" value="" name="email" placeholder="* E-mail" required="true" data-parsley-type="email" data-parsley-required="true">
                      </div>
                      <div class="col-md-12">
                        <input class="form-control input-field-text" type="tel" value="" name="phone" placeholder="* Phone number" required="true" data-parsley-pattern="/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/" data-parsley-required="true">
                      </div>
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="YES" name="sweepstakes_newsletter" checked="checked">Sign up for newsletter</label>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="" name="terms" checked="checked" required="true" data-parsley-required="true">Agree to terms and service</label>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <input class="btn btn-default form-control input-button" type="submit" value="Submit" name="send">
                      </div>
                    </div>
                  </form>
                  <p class="paragraph-sm text-center">Read our terms and conditions
                    <a style="text-decoration:underline" href="terms_condition.html" target="_blank">here</a> .</p>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      <div id="fb-root"></div>
      <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=902475459786648&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>
      <script>
        ! function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0],
            p = /^http:/.test(d.location) ? 'http' : 'https';
          if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
          }
        }(document, 'script', 'twitter-wjs');
      </script>
      <div class="social-media-bar-container">
        <ul class="social-media-bar">
          <li class="social">
            <h3>Follow us</h3>
            <ul class="nav nav-pills">
              <li>
                <div class="like">
                  <div class="fb-like" data-href="https://www.facebook.com/swedentravel" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                  <br />
                  <div class="fb-share-button" data-layout="button"></div>
                </div>
                <a href="https://www.facebook.com/swedentravel" class="popup"> <span class="icons icon-t3-facebook"></span>  </a> 
              </li>
              <li>
                <div class="like">
                  <a href="https://twitter.com/visitsweden" data-show-count="false" class="twitter-follow-button" data-show-screen-name="false">Follow</a> 
                  <br />
                  <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a> 
                </div>
                <a href="https://twitter.com/visitsweden" class="popup"> <span class="icons icon-t3-twitter"></span>  </a> 
              </li>
            </ul>
          </li>
          <li class="mail-form">
            <h3>Sign up for newsletter test</h3>
            <div class="visit-sweden-subscription">
              <form action="https://visitsweden.us7.list-manage.com/subscribe/post?u=c403aaad1aeaccc1c2d8be308&id=fc5639cff2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="form-group">
                  <input type="email" value="" name="EMAIL" class="form-control required email" id="mce-EMAIL" placeholder="Your e-mail" required="">
                </div>
                <div style="position: absolute; left: -5000px">
                  <input type="text" name="b_e23d7e5b89e722dab62b37430_66a1ef906b" tabindex="-1" value="">
                </div>
                <div class="subscribe-button">
                  <input type="submit" value="Yes please" name="subscribe" class="button" id="mc-embedded-subscribe">
                </div>
              </form>
            </div>
          </li>
        </ul>
      </div>

      <!--End mc_embed_signup-->
      <?php include 'inc-sponsors.php';?>
      <!-- <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <h2 class="text-center">Our partners</h2>
              <div class="row">
                <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
                  <a href="http://www.volvocars.com/us" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/volvo.jpg" alt="volvo">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
                <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
                  <a href="http://www.vastsverige.com/en/west-sweden/" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/westsweden.png" alt="west swede">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
                <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
                  <a href="http://www.goteborg.com/" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/goteborg.jpg" alt="gotenburg">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
                <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
                  <a href="http://www.flysas.com/" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/sas.jpg" alt="SAS">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      
      <?php include 'inc-summer-in-sweden.php';?>
      
      <!-- <div class="section dark hidden-xs">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">It's closer than you think</h3>
              <h2 class="text-center">Summer in Sweden</h2>
            </div>
          </div>
        </div>
        <ul class="visit-sweden-inspiration-feed">
          <li class="slice child-1">
            <a href="http://www.volvomuseum.com/" style="background-image:url(images/designmom-Volvo-Tour3.jpg)" target="_blank"></a> 
            <div class="content">
              <h3>Volvo Museum</h3>
              <p>A world of iconic car models, groundbreaking innovations and imaginative prototypes.</p>
              <a href="http://www.volvomuseum.com/" class="read-more" target="_blank">Read more</a> 
            </div>
          </li>
          <li class="slice child-2">
            <a href="http://www.vastsverige.com/en/products/52836/Lake-Hornborga/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n&utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n" style="background-image:url(images/Lake-Hornborga_7.jpg)"
            target="_blank"></a> 
            <div class="content">
              <h3>Lake Hornborga</h3>
              <p>This is where the dancing cranes are. Great spot to hike.</p>
              <a href="http://www.vastsverige.com/en/products/52836/Lake-Hornborga/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n&utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n" class="read-more"
              target="_blank">Read more</a> 
            </div>
          </li>
          <li class="slice child-3">
            <a href="http://www.goteborg.com/foodtrucks/" style="background-image:url(images/food-stall.jpg)" target="_blank"></a> 
            <div class="content">
              <h3>Food stalls</h3>
              <p>Try the food stalls in Magasinsgatan, Gothenburg.</p>
              <a href="http://www.goteborg.com/foodtrucks/" class="read-more" target="_blank">Read more</a> 
            </div>
          </li>
          <li class="slice child-4">
            <a href="http://www.vastsverige.com/en/products/47168/Lacko-Slott-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_L%C3%A4ck%C3%B6%20slott" style="background-image:url(images/Lacko-Castle-Kallandso_5.jpg)" target="_blank"></a> 
            <div class="content">
              <h3>Läckö Castle</h3>
              <p>Läckö Castle is best known as Magnus de la Gardie´s magnificent castle on the shores of Lake Vänern, but it is much older by far</p>
              <a href="http://www.vastsverige.com/en/products/47168/Lacko-Slott-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_L%C3%A4ck%C3%B6%20slott" class="read-more" target="_blank">Read more</a> 
            </div>
          </li>
          <li class="slice child-5">
            <a href="http://www.goteborg.com/en/universeum/" style="background-image:url(images/universum.jpg)" target="_blank"></a> 
            <div class="content">
              <h3>Universum</h3>
              <p>Universeum is located in the heart of Gothenburg and is Scandinavia's largest science centre.</p>
              <a href="http://www.goteborg.com/en/universeum/" class="read-more" target="_blank">Read more</a> 
            </div>
          </li>
        </ul>
      </div> -->

      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Bradt travel guides</h3>
              <h2 class="text-center">West Sweden including Gothenburg</h2>
              <div class="row">
                <div class="col-md-6">
                  <a href="http://www.amazon.com/West-Sweden-Including-Gothenburg-Regional/dp/1841625590" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/bradt.jpg" alt="buy book">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
                <div class="col-md-6">
                  <p>With a bewildering array of over 8,000 islands, endless meadows of wild flowers and the lively and cosmopolitan coastal city of Gothenburg, west Sweden perfectly encapsulates both the rugged beauty and urban delights Scandinavia has
                    to offer. In the first guidebook dedicated to the area, Bradt's West Sweden reveals the staggering variety of the area's experiences, includes a section on the region's history and culture and offers detailed maps of both the coastline
                    and the cities.</p>
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <p>
                        <a class="btn btn-default" href="http://www.amazon.com/West-Sweden-Including-Gothenburg-Regional/dp/1841625590" target="_blank">Buy book</a> 
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="visit-sweden-footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 visit-sweden-logo-top visit-sweden-white-border">
            <div class="row">
              <div class="col-xs-6 col-md-6">
                <div class="visit-sweden-logo-link visit-sweden-logo-left visit-sweden-logo-white-background">
                  <a href="index.php">
                    <img src="images/Visit_Sweden_logotype.svg" alt="" />
                  </a> 
                </div>
              </div>
              <div class="col-xs-6 col-md-6">
                <ul class="nav-pills visit-sweden-social-like-block">
                  <li>
                    <div class="like">
                      <div class="fb-like" data-href="https://www.facebook.com/swedentravel" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                      <br />
                      <div class="fb-share-button" data-layout="button"></div>
                    </div>
                    <a href="https://www.facebook.com/swedentravel" class="popup"> <span class="icons icon-t3-facebook"></span>  </a> 
                  </li>
                  <li>
                    <div class="like">
                      <a href="https://twitter.com/visitsweden" data-show-count="false" class="twitter-follow-button" data-show-screen-name="false">Follow</a> 
                      <br />
                      <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a> 
                    </div>
                    <a href="https://twitter.com/visitsweden" class="popup"> <span class="icons icon-t3-twitter"></span>  </a> 
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-12 footer-about">
                            <h4>Visit Sweden</h4>
                            <p>Visit Sweden is the official marketing organization for Sweden as a travel destination.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 visit-sweden-list visit-sweden-white-arrow-list">
                        <h4>Quick Links</h4>

                        <?php include 'inc-footer-menu.php';?>
                        <!-- <ul>
                          <li> <a href="travellers.html">Travellers</a>  </li>
                          <li> <a href="contest.html">Sweepstakes</a>  </li>
                          <li> <a href="activities.html">Activities</a>  </li>
                        </ul> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 visit-sweden-list">
                    <h4>Sign up for our newsletter</h4>
                    <p>Get inspiration, offers and participate in competitions where you can win experiences for life. Sign up for our newsletter by typing in your email address below.</p>
                    <div class="visit-sweden-subscription">
                      <form action="http://visitsweden.us7.list-manage.com/subscribe/post?u=c403aaad1aeaccc1c2d8be308&id=fc5639cff2" method="post" id="mc-embedded-subscribe-form-footer" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="" _lpchecked="1">
                        <div class="form-group">
                          <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Your e-mail   " required="">
                        </div>
                        <div style="position: absolute; left: -5000px">
                          <input type="text" name="b_c403aaad1aeaccc1c2d8be308_fc5639cff2" tabindex="-1" value="">
                        </div>
                        <div class="subscribe-button">
                          <input type="submit" value="Yes please" name="subscribe" class="button">
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-12 footer-copyright-description">
                    <div class="row">
                      <div class="col-md-12">
                        <p>
                          <b>COPYRIGHT 2015 VISITSWEDEN
                            <br />ALL RIGHTS RESERVED.
                            <a href="http://www.visitsweden.com/" target="_blank">VISITSWEDEN.COM</a> 
                          </b>
                        </p>
                      </div>
                      <div class="col-md-6">
                        <p>
                          <b>ADMINISTERED BY VISITSWEDEN.</b>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 visit-sweden-sender-model visit-sweden-sender-model-white">
            <div class="visit-sweden-logo-link">
              <a href="http://sweden.se" target="_blank">
                <img src="images/sweden-logo.svg" alt="">
              </a> 
            </div>
            <div class="visit-sweden-logo-link">
              <a href="http://www.visitsweden.com/" target="_blank">
                <img src="images/visit-logo.svg" alt="">
              </a> 
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

      <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
      <div class="pswp__bg"></div>

      <!-- Slides wrapper with overflow:hidden. -->
      <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">

            <!--  Controls are self-explanatory. Order can be changed. -->
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

            <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->

            <!-- element will get class pswp__preloader--active when preloader is running -->
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
          </button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
          </button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <script src="main.js"></script>
    <script src="local.js"></script>
    <script type="text/javascript" src="//cdn.resultify.com/clients/visitsweden_new_york/"></script>

    <script>
      $(document).ready(function(){
        var activeMenu = $('.navbar-nav a[href^="contest"]:first')
        $(activeMenu).parent().addClass('active');
      });
    </script>


  </body>
</html>
