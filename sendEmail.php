<?php

$args = $_POST;
$args['email'] = preg_replace('=((<CR>|<LF>|0x0A/%0A|0x0D/%0D|\\n|\\r)\S).*=i', null, $args['email']);

if (!$args['email'] || !$args['name'] || !$args['phone'])
    die('ERROR');

if($args['sweepstakes_newsletter'] == null)
    $args['sweepstakes_newsletter'] = "NO";

error_log(print_r($args, true));

require('MailChimp.php');
$MailChimp = new \Drewm\MailChimp('883f7d50ae5e948a6b1c29e0f00c2e24-us7');
$result = $MailChimp->call('lists/subscribe', array(
                'id'                => 'fc5639cff2',
                'email'             => array('email'=>$args['email']),
                'merge_vars'        => array('FULLNAME'=>$args['name'], 'PHONE'=>$args['phone'], 'MMERGE2'=>'NO', 'NEWSLETTER'=> $args['sweepstakes_newsletter']),
                'double_optin'      => false,
                'update_existing'   => false,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));

if ( ! empty( $result['leid'] ) ) {
   echo "OK";
}
else
{
    echo "ERROR";
}

?>
