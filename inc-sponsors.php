<style>
  @media(max-width: 760px){
    .visit-sweden-partner-link .image{
      margin-bottom:0px;
    }
    .visit-sweden-partner-link .image img{
      width:60%;
      height:auto;
    }
  }
</style>

<div class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h2 class="text-center">Our partners</h2>
        <div class="row">
          <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
            <a href="http://www.volvocars.com/us" target="_blank">
              <div class="image-center">
                <div class="image-wrap">
                  <div class="image-center-outer">
                    <div class="image-center-inner">
                      <figure class="image">
                        <img src="images/volvo.jpg" alt="volvo">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </a> 
          </div>
          <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
            <a href="http://www.vastsverige.com/en/west-sweden/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Startsida_2015" target="_blank">
              <div class="image-center">
                <div class="image-wrap">
                  <div class="image-center-outer">
                    <div class="image-center-inner">
                      <figure class="image">
                        <img src="images/westsweden.png" alt="west swede">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </a> 
          </div>
          <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
            <a href="http://www.goteborg.com/en/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">
              <div class="image-center">
                <div class="image-wrap">
                  <div class="image-center-outer">
                    <div class="image-center-inner">
                      <figure class="image">
                        <img src="images/goteborg.jpg" alt="gotenburg">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </a> 
          </div>
          <div class="col-xs-6 col-md-3 visit-sweden-partner-link">
            <a href="http://www.flysas.com/" target="_blank">
              <div class="image-center">
                <div class="image-wrap">
                  <div class="image-center-outer">
                    <div class="image-center-inner">
                      <figure class="image">
                        <img src="images/sas.jpg" alt="SAS">
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            </a> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>