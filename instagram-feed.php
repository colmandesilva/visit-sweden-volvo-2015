<?php
// Client ID for Instagram API
$instagramClientID = '1c0143e145624343bd5e727c7e832847';
$tag = 'inavolvo';

//$api = 'https://api.instagram.com/v1/media/popular?client_id='.$instagramClientID; //api request (edit this to reflect tags)
$api = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?client_id='.$instagramClientID; 
$cache = './cache.json';

if(file_exists($cache) && filemtime($cache) > time() - 60*60){
    // If a cache file exists, and it is newer than 1 hour, use it
    $images = json_decode(file_get_contents($cache),true); //Decode as an json array
    print_r(json_encode($images));
}
else{
    // Make an API request and create the cache file
    // For example, gets the 32 most popular images on Instagram
    $response = get_curl($api); //change request path to pull different photos

    $images = array();

    if($response){
        // Decode the response and build an array
        foreach(json_decode($response)->data as $item){

            // Makes sure that inavolvo result also contains the tag westsweden OR gothenburg
            if (in_array("westsweden", $item->tags) || in_array("gothenburg", $item->tags)) {
                
                $title = (isset($item->caption))?mb_substr($item->caption->text,0,100,"utf8"):null;
 
                $src = $item->images->standard_resolution->url; //Caches standard res img path to variable $src

                //Location coords seemed empty in the results but you would need to check them as mostly be undefined
                $lat = (isset($item->data->location->latitude))?$item->data->location->latitude:null; // Caches latitude as $lat
                $lon = (isset($item->data->location->longtitude))?$item->data->location->longtitude:null; // Caches longitude as $lon
                
                $user = $item->user->username;
                $link = $item->link;

                $images[] = array(
                "title" => htmlspecialchars($title),
                "src" => htmlspecialchars($src),
                "lat" => htmlspecialchars($lat),
                "lon" => htmlspecialchars($lon), // Consolidates variables to an array
                "user" => htmlspecialchars($user),
                "link" => htmlspecialchars($link)
                );
            }
        }
        file_put_contents($cache,json_encode($images)); //Save as json
        print_r(json_encode($images));
    }
}

//Debug out
//print_r(json_encode($images));

//Added curl for faster response
function get_curl($url){
    if(function_exists('curl_init')){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        $output = curl_exec($ch);
        echo curl_error($ch);
        curl_close($ch);
        return $output;
    }else{
        return file_get_contents($url);
    }

}

?>