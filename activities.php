<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimal-ui" />
    <title>Activities</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/android-chrome-manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script>
      // touchSupport
      touchSupport = (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
      if (!touchSupport) {
        document.documentElement.className += " no-touch";
      } else {
        document.documentElement.className += " touch";
      }
       // IE version
      var uA = window.navigator.userAgent;
      var msie = uA.indexOf('MSIE ');
      var trident = uA.indexOf('Trident/');
      var ie;
      function detectIE() {
        if (msie > 0) {
          // IE 10 or older => return version number
          ie = 'ie' + parseInt(uA.substring(msie + 5, uA.indexOf('.', msie)), 10);
          return ie;
        }
        if (trident > 0) {
          // IE 11 (or newer) => return version number
          var rv = uA.indexOf('rv:');
          ie = 'ie' + parseInt(uA.substring(rv + 3, uA.indexOf('.', rv)), 10);
          return ie;
        }
        return ie = 'nonIE';
      }
      detectIE();
      if (ie === 'nonIE') {
        document.documentElement.className += (' ' + ie);
      } else {
        document.documentElement.className += (' ' + (ie + ' ' + 'IE'));
      };
       // Google Analytics tracking code
      (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-410969-1', 'auto');
      ga('send', 'pageview');
    </script>
    
    
    <link rel="stylesheet" href="main.css">
  </head>
  <body>

    <!--[if lt IE 9]>
      <p class="browsehappy">You are using an <strong>outdated</strong>  browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>  to improve your experience.</p>
            <![endif]-->
    <header class="visit-sweden-header">
      <nav class="navbar navbar-default  visit-sweden-main-nav-root-btn" role="navigation">
        <div class="container">
          <a class="visit-sweden-logo" href="index.php" title="To startpage">
            <img src="images/Visit_Sweden_logotype-negative2.svg" alt="logo">
            <p>In a
              <br>Volvo</p>
          </a> 
          <div class="navbar-header">
            <button type="button" class="navbar-toggle visit-sweden-nav-toggle-button collapsed" data-toggle="collapse" data-target="#visit-sweden-root-main-navbar-collapse">
              <span class="sr-only">Toggle navigation</span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="visit-sweden-root-main-navbar-collapse">
            <div id="visit-sweden-nav-search-collapse" class="visit-sweden-main-search-collapse collapse">
              <div class="container">
                <form class="search" role="search" action="search.html">
                  <span class="icons icon-t3-search"></span> 
                  <div class="form-group" id="the-basics">
                    <input type="search" class="form-control typeahead quicksearch" placeholder="Search">
                    <button type="submit" style="display:none"></button>
                  </div>
                </form>
              </div>
            </div>

            <?php include 'inc-menu.php';?>
            <!-- <ul class="nav navbar-nav">
              <li> <a href="index.php">Home</a>  </li>
              <li> <a href="contest.html">Contest</a>  </li>
              <li> <a href="travellers.html">Travellers</a>  </li>
              <li class="active">
                <a href="activities.html">Activities</a> 
              </li>
            </ul> -->
          </div>
        </div>
      </nav>
      <div id="pxa-search-collapse" class="pxa-main-search-collapse collapse ">
        <div class="container">
          <form class="search" role="search" action="search.html">
            <span class="icons icon-t3-search"></span> 
            <div class="form-group" id="the-basics">
              <input type="search" class="form-control typeahead quicksearch" placeholder="Search">
              <button type="submit" style="display:none"></button>
            </div>
          </form>
        </div>
      </div>
    </header>
    <div class="visit-sweden-page100">
      <div class="visit-sweden-header-block" style="background-image: url(images/header-bloggers.jpg);">
        <div class="visit-sweden-header-block-content visit-sweden-logo-top visit-sweden-white-border">
          <div class="visit-sweden-logo-link visit-sweden-logo-left visit-sweden-logo-white-background">
            <a href="index.php">
              <img src="images/Visit_Sweden_logotype.svg" alt="" />
            </a> 
          </div>
          <div class="visit-sweden-header-block-description">
            <p>Discover</p>
            <h1>The beauty<br/>of Sweden</h1>
          </div>
        </div>
      </div>
      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Exploring</h3>
              <h2 class="text-center">Gothenburg & West Sweden</h2>
              <div class="visit-sweden-activities-isotope">
                <section id="options" class="options">
                  <div class="option-combo">
                    <div id="filter" class="filter option-set" data-option-key="filter">
                      <a href="#show-all" class="btn btn-default selected" data-option-value="*">All</a>  
                      <a href="#Adventure" class="btn btn-default" data-option-value=".Adventure">Adventure</a> 
                      <a href="#Culture" class="btn btn-default" data-option-value=".Culture">Culture</a> 
                      <a href="#Eco" class="btn btn-default" data-option-value=".Eco">Eco</a> 
                      <a href="#Eats" class="btn btn-default" data-option-value=".Eats">Eats</a>
                      <a href="#Stay" class="btn btn-default" data-option-value=".Stay">Stay</a> 

                    </div>
                  </div>
                </section>

                <!-- elements -->
                <div class="isotope-container clearfix">
                  <div class="grid-sizer"></div>
                  <div class="gutter-sizer"></div>

                  <div class="isotope-item Adventure The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/djelson-mtb.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Delsjön Mountain biking</h4>
                      <p class="bodytext">Try mountainbiking in the forest only 10 minutes from Gothenburg city center.</p>
                      <a class="btn btn-default" href="http://www.hillsidecycling.com/" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/rosenkafeet.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Rosenkaféet</h4>
                      <p class="bodytext">Rosenkaféet is an oasis in the middle of the city. It is located in the Garden Society's oldest building dating back to 1874.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/rosenkafeet/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Stay The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/STF-hotel.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>STF GÖTEBORG CITY HOTEL & HOSTEL</h4>
                      <p class="bodytext">Centrally located budget accomodation.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/stf-goteborg-city-hotell--vandrarhem/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Stay The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/styrso-skaret.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>PENSIONAT STYRSÖ SKÄRET</h4>
                      <p class="bodytext">Here everything from the facade of the room decor exudes genuine archipelago's history.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/pensionat-styrso-skaret/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/fiskeboa.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>FISKEBOA</h4>
                      <p class="bodytext">Some people travel across the world to find great culinary experiences and fancy restaurants. Other people takes the ferry to the island Vrångö.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/fiskeboa/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Stay The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/strandflickornas-havshotell.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Strandflickornas Havshotell</h4>
                      <p class="bodytext">Your stay in this charming boutique hotel from the turn of the century, with its own little piers and the beautiful outdoor areas will give you a wonderful memory.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/45863/Strandflickornas-Havshotell-Lysekil/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Strandflickornashavshotell_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Stay The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/hotel-smogen.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Smögens Hafvsbad</h4>
                      <p class="bodytext">Smögens Hafvsbad is an integral part of the history of the island.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/46028/Hotel-Smogens-Hafvsbad/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Smogenshafvsbad_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/weather-islands.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Weather Islands</h4>
                      <p class="bodytext">The islands have been described as being "A heaven in the sea or a hell on earth".</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/22466/Vaderoarnas-Vardshus-Konferens/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Weatherislands_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/made-in-china.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Made in China</h4>
                      <p class="bodytext">Tasty dim sum, but without the formality of fine dining. Great Asian food to share with friends.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/made-in-china/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/everts-sjobod.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Everts Sjöbod</h4>
                      <p class="bodytext">Oystersafari in Grebbestad</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/92621/Oyster-Tasting-at-Everts-Sjobod/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Evertssjobod_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/encota-maglia.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>ENOTECA MAGLIA</h4>
                      <p class="bodytext">A modern restaurant inspired by Italy, located in the cosy neighbourhood at Mariaplan in Majorna.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/enoteca-maglia/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/alvar-ivar.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>ALVAR & IVAR SURDEGSBAGERI</h4>
                      <p class="bodytext">Bread the way it should be. A small neighborhood bakery in Linnéstaden.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/alvar--ivar-surdegsbageri/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/cumpane.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Cum Pane Bakery</h4>
                      <p class="bodytext">A nice little bakery in the lovely Majorna district.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/cum-pane/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/cafe-husaren.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>CAFÉ HUSAREN</h4>
                      <p class="bodytext">Café famous for their enormous cinnamon buns.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/cafe-husaren/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/marstrand-havshotell.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Marstand Havshotell</h4>
                      <p class="bodytext">Marstrand is an ancient place where all the qualities of the west coast of Sweden merge.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/114645/Marstrands-Havshotell-We-are-not-like-the-others/" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/meettheswedes.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Meet the Swedes</h4>
                      <p class="bodytext">Experience Sweden and Swedes in a new way.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/133993/Meet-the-Swedes/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_MeettheSwedes_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <!-- <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/karrigon-oysterbar.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Käringö Oysterbar</h4>
                      <p class="bodytext">A truly unique oyster bar located on Käringön, overlooking the Måseskär lighthouse in the Bohuslän archipelago</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/114982/Karingo-Oyster-bar-Karingon/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Karingooysterbar_2015" target="_blank">Learn more</a> 
                    </div>
                  </div> -->

                  <div class="isotope-item Stay The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/karringon.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Käringön</h4>
                      <p class="bodytext">Offering a variety of housing alternatives.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/45807/Hotel-Karingon-Karingon/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_hotelkaringo_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/riverton.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Riverton View Skybar & Restaurant</h4>
                      <p class="bodytext">Swedish/Asian fusion with a stunning view.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/riverton-view-skybar-and-restaurant/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/thebarn.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>The Barn</h4>
                      <p class="bodytext">High quality burgers and beer.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/the-barn/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/dorsia.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Dorsia</h4>
                      <p class="bodytext">A hotel for true conoisseurs.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/dorsia/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/fossenoberg.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>FORSSÉN & ÖBERG</h4>
                      <p class="bodytext">Champagne bar with a lovely courtyard.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/forssen--oberg/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/heaven23.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Heaven 23</h4>
                      <p class="bodytext">Cocktail bar and restaurant on the 23rd floor with amazing views of Gothenburg.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/heaven-23/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/time-travel.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Time Travel Sightseeing</h4>
                      <p class="bodytext">Cruise around Gothenburg in a classic Volvo PV.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/time-travel-sightseeing/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/sjomagasinet.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Sjömagasinet</h4>
                      <p class="bodytext">Michelin-starred restaurant with flair for seafood.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/sjomagasinet/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/roda-sten.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Röda Sten Art Centre</h4>
                      <p class="bodytext">Contemporary art, theatre, music and dance.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/roda-sten-konsthall/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  
                  <div class="isotope-item Eats The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/restaurant-aterlier.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Restaurant Atelier</h4>
                      <p class="bodytext">Restaurant with a flair for early 20th century Paris.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/restaurant-atelier/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>


                  <div class="isotope-item Stay The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/hotel-rubinen.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Scandic Rubinen</h4>
                      <p class="bodytext">Hotel with a fantastic roof terrace right on the main boulevard.</p>
                      <a class="btn btn-default" href=" http://www.goteborg.com/en/scandic-rubinen/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/bhoga_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Bhoga</h4>
                      <p class="bodytext">Creative Scandinavian cuisine with one Michelin star.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/bhoga/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Bar-kino_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>HAGABIONS CAFÉ OCH BAR KINO</h4>
                      <p class="bodytext">Vegetarian food, local beer and lots of hipsters.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/hagabions-cafe-och-bar-kino/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Kosters-tradgard-01-Foto-Asa-Dahlgren_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>The Koster Gardens</h4>
                      <p class="bodytext">An organic garden restaurant they only cook the day's fresh catch, garnished with herbs and vegetables they grow themselves.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/19026/Kosters-Tradgardar/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Kosters%20tr%C3%A4dg%C3%A5rdar" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats The-Foodie The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/sk-mat_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>SK MAT OCH MÄNNISKOR</h4>
                      <p class="bodytext">Swedish cuisine with a modern touch.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/sk-mat-och-manniskor/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/food-stall_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">  
                      <h4>Magasinsgatan</h4>
                      <p class="bodytext">Gothenburg’s street food flora has blossomed thanks to the food trucks and street food places that can be found across the city</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/foodtrucks/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats Culture The-Foodie The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Bjertorp-manor_5_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Bjertorp Manor</h4>
                      <p class="bodytext">The manor offers a setting and feeling one might place in a time long since past.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/16346/Bjertorp-Manor/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Bjertorp" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Restaurang-Sjoboden-in-Kallandso_1_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Restaurant Sjöboden</h4>
                      <p class="bodytext">Restaurang Sjöboden sits at the tip of a headland called Kållandsö that juts into Lake Vänern, Sweden's biggest lake and also one of Europe's largest.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/22749/Restaurang-Sjoboden-in-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Sj%C3%B6boden" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/smaklust-2009_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Qvänum Mat & Malt</h4>
                      <p class="bodytext">Qvänum Mat & Malt is a brewery/distillery and tastings and guesthouse establishment midway between the towns of Skara and Vara in West Sweden.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/48029/Qvanum-Mat-Malt/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Qv%C3%A4num%20Mat%20och%20Malt" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats The-Foodie The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Knack-brack_5_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Knäck & Bräck</h4>
                      <p class="bodytext">As well as being good for you, our crisp breads give you a tasty, eating experience.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/126122/Knack-Brack/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Kn%C3%A4ck%20och%20Br%C3%A4ck" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats Adventure Culture The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Oysster-and-Mussel-Tour-in-Lysekil_6_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Oyster and Mussel tour</h4>
                      <p class="bodytext">Oyster and Mussel Tour with a scent of seaweed and freshly cooked mussels</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/148886/Oyster-and-Mussel-Tour-in-Lysekil/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Lysekils%20Ostron%20och%20musslor" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats Adventure Stay The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Gullmarsstrand-hotel-in-fiskebackskil_3_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Gullmarsstrand</h4>
                      <p class="bodytext">So close to the water as you can get on on elongated bridge overlooking Gullmarsfjorden islands and bays.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/45598/Gullmarsstrand-Hotell-Konferens/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Gullmarsstrand" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Adventure Culture Eco The-Explorer The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Universum_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Universeum Science Discovery Center</h4>
                      <p class="bodytext">Explore 9000 square meters of adventure!</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/universeum/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  
                  <!-- <div class="isotope-item Adventure Eco The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/djelson_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Nature Reserve</h4>
                      <p class="bodytext">A popular nature area for swimming and hiking close to the city centre</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/delsjon/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div> -->


                  <div class="isotope-item Adventure The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/cyklande-par-vid-sjo-8094_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Biking in Dalsland</h4>
                      <p class="bodytext">Enjoy Dalsland form a bicycle. There are many nice bike paths.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/73807/Silverlake-Base-Camps-combination-package/?utm_source=inavolvo.com&utm_medium=link&utm_campaign=inavolvo_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Adventure The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/kanotpaddling-94_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Dalsland</h4>
                      <p class="bodytext">Canoe in the lakes, visit the moosepark and spend the night in a tipi</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/16946/Dalsland-Activities/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Explorer_Dalslandsaktiviteter" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Adventure The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Kayaking-Bohuslan-coast-26924_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Seakayak</h4>
                      <p class="bodytext">Seakayak paddling in Lysekil with Nautopp</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/73114/Nautopp-Seakayaking-Sweden/" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Adventure The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Bergsklattring-30414_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Smögen</h4>
                      <p class="bodytext">Hiking on granite ricks and outdoor lunch in charming Smögen area.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/Global-search-results/?q=Sm%C3%B6gen" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  
                  <div class="isotope-item Adventure Eco Culture The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/djelson_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>DELSJÖN</h4>
                      <p class="bodytext">A popular nature area for swimming and hiking close to the city centre.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/delsjon/" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Adventure Culture The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/ferry_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Styrsö Island</h4>
                      <p class="bodytext">Styrsö can be considered the hub in the southern part of the archipelago</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/styrso/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Foodie2 The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/museum-art_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>GOTHENBURG MUSEUM OF ART</h4>
                      <p class="bodytext">Art collections from the 15th century to today.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/goteborgs-konstmuseum/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Foodie2 The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/fjallbacka_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Fjällbacka</h4>
                      <p class="bodytext">Home town of crime writer Camilla Läckberg and past summer destination for Ingrid Bergman.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/fjallbacka-skargard/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Curator_Fj%C3%A4llbacka" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Rorstrands-Museum-Lidkoping_6_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Rörstrand's Museum</h4>
                      <p class="bodytext">The museum opened in March 2008, with a spectacular exhibition designed by White Architects</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/51367/Rorstrands-Museum-Lidkoping/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Curator_R%C3%B6rstrands%20museum" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Curator The-Adventuer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/strandverket_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>STRANDVERKET ART GALLERY</h4>
                      <p class="bodytext">Contemporary art of the highest international standard.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/strandverket-konsthall/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Sculpture-at-Pilane-2014_2_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Sculpture Park</h4>
                      <p class="bodytext">Animals, nature and sculpture - Sculpture at Pilane Tjörn is an experience for everyone, young and old alike.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/110035/Sculpture-at-Pilane-2013/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Curator_Pilane" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/vitlycke-museum-tranumshed_1_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Vitlycke museum</h4>
                      <p class="bodytext">Vitlycke museum is the information and knowledge centre of the World Heritage Tanum's rock carvings in North Bohuslän</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/49993/Vitlycke-museum-Tanumshede/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Curator_Vitlycke" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/rohska-museum_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>THE RÖHSSKA MUSEUM</h4>
                      <p class="bodytext">Fashion, design and decorative arts.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/rohsska-museet/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eats Culture Stay Eco The-Foodie2 The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Norrqvarn-04_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Norrqvarn</h4>
                      <p class="bodytext">This is a lovely destination where, apart from Norrqvarn Hotel and Conference with its restaurant, café, hotel and hostel, there are locks, a cultural pathway, a children's mini-canal, and magic tree stumps for overnight stays.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/45723/Norrqvarn-Hotell-Konferens-Restaurant/?utm_source=inavolvo.com&utm_medium=link&utm_campaign=inavolvo_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Salsafari-i-bohuslan-23072_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Seal Safari</h4>
                      <p class="bodytext">A 3-hour trip to look at seals, on comfortable and spacious M/S Sara</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/92792/Seal-Safari-with-MS-Sara/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Seal%20safari%20Kosterhavet" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  
                  <div class="isotope-item Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/Naturum_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Naturum</h4>
                      <p class="bodytext">A naturum is a visitor centre with activities and exhibitions, located at several of Sweden’s national parks and nature reserves.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/Stromstad-Koster/products/159399/Information-center-Kosterhavet/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_Kosterhavet_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats Eco Stay The-Eco-Tourist New-Blogger3 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/hotel-koster_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Koster</h4>
                      <p class="bodytext">Hotel with bar and restaurant, which is situated on the cliff facing east, 100 metres from the harbour and pier of Ekenäs on the island of South Koster outside of Strömstad.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/91965/Ekenas-Hotel-Sydkoster-Stromstad/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Sydkoster%20Eken%C3%A4s" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eco Stay The-Foodie2 The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/tree_hotel_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Tree Hotel</h4>
                      <p class="bodytext">Whether you prefer the small, charming Breathing Space or the beautiful 7th Heaven, you will sleep well to the sound of the wind rustling the large branches of the oak tree.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/49570/Tree-hotels-Hotell-Andrum-and-7e-Himlen/" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eco The-Foodie2 The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/gota-biking_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>The Göta Canal</h4>
                      <p class="bodytext">The Göta Canal is one of the largest construction projects ever undertaken in Sweden. Great area for picnic and bike rides.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/Experience-Gota-Canal/products/51368/Two-day-Gota-Canal-bike-package-including-accommodation-68-km/" target="_blank">Learn more</a> 
                    </div>
                  </div>


                  <!-- Added by Koalition : START -->

                  <div class="isotope-item Eco Culture The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/styr_stall_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>STYR & STÄLL</h4>
                      <p class="bodytext">Rental bikes at more than 50 stations spread around town.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/styr-och-stall/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture Eco Stay The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/hotel_lisberg_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Liseberg Heden</h4>
                      <p class="bodytext">Enviromentrally certified and centrally located four-star hotel</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/hotell-liseberg-heden/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture Eco The-Foodie The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/gunnebo_gardens_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Gunnebo House and Gardens</h4>
                      <p class="bodytext">A magnificently well maintained 18th-century timber building and grounds in a rural setting</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/45610/Gunnebo-House-and-Gardens/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Gunnebo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Foodie2 The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/castle_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Läckö Castle</h4>
                      <p class="bodytext">Läckö Castle is best known as Magnus de la Gardie´s magnificent castle on the shores of Lake Vänern, but it is much older by far</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/47168/Lacko-Slott-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_L%C3%A4ck%C3%B6%20slott" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture Stay The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/victoriahuset_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Victoriahuset</h4>
                      <p class="bodytext">The naturum now stands complete in the middle of Lake Vänern's natural and cultural centre beside Läckö Castle, a symbolic gift to H.R.H. Crown Princess Victoria on her thirtieth birthday</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/136834/Overnight-stays-at-naturum-Vanerskargarden-Victoriahuset/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Victoriahuset%20" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/sivans_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Sivans Cheese</h4>
                      <p class="bodytext">This lady knows how to mature cheeses!</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/53702/Sivans-Osthandel-in-Vara/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_Sivans%20Ost" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Foodie The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/paddan_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Paddan</h4>
                      <p class="bodytext">Sightseeing along the canals of Gothenburg</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/paddan/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/puta_madre_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Puta Madre</h4>
                      <p class="bodytext">Mexican restaurant with an interior design out of the ordinary</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/puta-madre/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats Culture The-Foodie The-Foodie2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/fish_auction_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Fish Auction</h4>
                      <p class="bodytext">See how fish is sold and purchased in high tempo.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/goteborgs-fiskeauktion/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats Stay The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/clarion_hotel_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Clarion Hotel Post</h4>
                      <p class="bodytext">Home of the famous pastry chef - Patrik Fredriksson</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/clarion-hotel-post/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/upper_house_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Upper House</h4>
                      <p class="bodytext">Great ambitions, a spectacular view and world class food</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/upper-house/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/villa_sjotorp_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Villa Sjötorp</h4>
                      <p class="bodytext">Villa Sjötorp's well-renowned kitchen offers culinary specialities made from local produce. Everything we serve is prepared in our own kitchen, mainly with locally produced ingredients.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/93032/Hotel-Villa-Sjotorp-Uddevalla/?utm_source=inavolvo.com&utm_medium=link&utm_campaign=inavolvo_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Curator The-Foodie2 The-Explorer The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/gabriel_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Restaurant Gabriel</h4>
                      <p class="bodytext">Seafood institution on the top floor of the market hall Feskekôrka.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/restaurang-gabriel/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/baldernas_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Baldersnäs Herrgård</h4>
                      <p class="bodytext">The mansions two hundred year history dates back to the iron mills</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/128378/Baldersnas-Herrgard-an-oasis-in-Dalsland/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Explorer_Baldersn%C3%A4s" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/norra_hamnen_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Norra Hamnen 5</h4>
                      <p class="bodytext">Let yourself be tempted by seafood of the highest quality.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/132901/Restaurang-Norra-Hamnen-5/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Explorer_Norra%20Hamnen" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/sealogde_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Sea Logde</h4>
                      <p class="bodytext">Local lobster, langoustine, oysters, crab, prawn, fish...need we say more?</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/70299/Sea-Lodge/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Explorer_Sealodge" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Foodie The-Foodie2 The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/norda_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Norda Bar & Grill</h4>
                      <p class="bodytext">A superb mix of Swedish and American cooking</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/norda-bar--grill/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Eats The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/barabicu_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Barabicu</h4>
                      <p class="bodytext">Pan American BBQ and classic cocktails à la New Orleans.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/barabicu/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Culture The-Foodie The-Explorer visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/city_walk_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>City Walk</h4>
                      <p class="bodytext">Haga is one of the oldest neighborhoods in Gothenburg</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/haga/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>


                  <div class="isotope-item The-Curator The-Explorer The-Foodie2 Eats visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/familjen.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Restaurant Familjen</h4>
                      <p class="bodytext">Restaurang Familjen is a contemporary West Swedish brasserie with a warm and friendly atmosphere.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/familjen/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>    


                  <div class="isotope-item Culture Stay The-Foodie2 The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/hotel-avalon.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Hotel Avalon</h4>
                      <p class="bodytext">Avalon Hotel is a feng-shui certified hotel where the decor, candles, scent and sound play a major role</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/avalon/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>


                  <div class="isotope-item Eats Stay The-Curator visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/salt-and-sill.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Salt & Sill</h4>
                      <p class="bodytext">Salt & Sill is proud of owning Sweden's first floating hotel. It offers visitors a peaceful environment with guaranteed sea views over Bohuslän's outer archipelago.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/sodra-bohuslan/products/76711/Floating-hotel-Salt-Sill/?utm_source=Volvo%20VisitSweden&utm_medium=Banner&utm_campaign=Volvo_SaltoSill_2015" target="_blank">Learn more</a> 
                    </div>
                  </div>


                  <div class="isotope-item Adventure The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/vrango_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Island of Vrångö</h4>
                      <p class="bodytext">Vrångö is the most southerly island of the Gothenburg archipelago</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/vrango/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <div class="isotope-item Adventure The-Explorer The-Explorer2 visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/city_kayak_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Kayak</h4>
                      <p class="bodytext">Kayak through the canals of Gothenburg</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/point-65-kayak-center/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>

                  <!-- Added by Koalition : END -->

                  <div class="isotope-item Culture Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/cyckling-on-south-koster_0_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>South Koster</h4>
                      <p class="bodytext">There are two Koster Islands, North – and South Koster, surrounded by a large number of rocks and islets. South Koster is the biggest; here the best way to get around is by walking or cycling.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/166245/Cycling-on-South-Koster/" target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/lake-Hornborga_7_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Lake Hornborga</h4>
                      <p class="bodytext">Famed as a bird lake, ravaged by countless attempts to lower the level of the water, Lake Hornborga was revived by an extensive and unique restoration programme.</p>
                      <a class="btn btn-default" href="http://www.vastsverige.com/en/products/52836/Lake-Hornborga/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n&utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n"
                      target="_blank">Learn more</a> 
                    </div>
                  </div>
                  <div class="isotope-item Culture Eco The-Eco-Tourist visit-sweden-ny-blogger image-center">
                    <div class="image-wrap">
                      <div class="image-center-outer">
                        <div class="image-center-inner">
                          <figure class="image">
                            <img src="images/liseberg_retina.jpg" alt="">
                          </figure>
                        </div>
                      </div>
                    </div>
                    <div class="text">
                      <h4>Liseberg</h4>
                      <p class="bodytext">Scandinavia's largest amusement park. Uses only energy from renewable resources.</p>
                      <a class="btn btn-default" href="http://www.goteborg.com/en/liseberg/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" target="_blank">Learn more</a> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="fb-root"></div>
      <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=902475459786648&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>
      <script>
        ! function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0],
            p = /^http:/.test(d.location) ? 'http' : 'https';
          if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
          }
        }(document, 'script', 'twitter-wjs');
      </script>
      <div class="social-media-bar-container">
        <ul class="social-media-bar">
          <li class="social">
            <h3>Follow us</h3>
            <ul class="nav nav-pills">
              <li>
                <div class="like">
                  <div class="fb-like" data-href="https://www.facebook.com/swedentravel" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                  <br />
                  <div class="fb-share-button" data-layout="button"></div>
                </div>
                <a href="https://www.facebook.com/swedentravel" class="popup"> <span class="icons icon-t3-facebook"></span>  </a> 
              </li>
              <li>
                <div class="like">
                  <a href="https://twitter.com/visitswedenus" data-show-count="false" class="twitter-follow-button" data-show-screen-name="false">Follow</a> 
                  <br />
                  <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a> 
                </div>
                <a href="https://twitter.com/visitswedenus" class="popup"> <span class="icons icon-t3-twitter"></span>  </a> 
              </li>
            </ul>
          </li>
          <li class="mail-form">
            <h3>Sign up for newsletter</h3>
            <div class="visit-sweden-subscription">
              <form action="http://visitsweden.us7.list-manage.com/subscribe/post?u=c403aaad1aeaccc1c2d8be308&id=fc5639cff2" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="form-group">
                  <input type="email" value="" name="EMAIL" class="form-control required email" id="mce-EMAIL" placeholder="Your e-mail" required="">
                </div>
                <div style="position: absolute; left: -5000px">
                  <input type="text" name="b_e23d7e5b89e722dab62b37430_66a1ef906b" tabindex="-1" value="">
                </div>
                <div class="subscribe-button">
                  <input type="submit" value="Yes please" name="subscribe" class="button" id="mc-embedded-subscribe">
                </div>
              </form>
            </div>
          </li>
        </ul>
      </div>

      <!--End mc_embed_signup-->
      <?php include 'inc-sponsors.php';?>

      <?php include 'inc-summer-in-sweden.php';?>

      
      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Bradt travel guides</h3>
              <h2 class="text-center">West Sweden including Gothenburg</h2>
              <div class="row">
                <div class="col-md-6">
                  <a href="http://www.amazon.com/West-Sweden-Including-Gothenburg-Regional/dp/1841625590" target="_blank">
                    <div class="image-center">
                      <div class="image-wrap">
                        <div class="image-center-outer">
                          <div class="image-center-inner">
                            <figure class="image">
                              <img src="images/bradt.jpg" alt="buy book">
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a> 
                </div>
                <div class="col-md-6">
                  <p>With a bewildering array of over 8,000 islands, endless meadows of wild flowers and the lively and cosmopolitan coastal city of Gothenburg, west Sweden perfectly encapsulates both the rugged beauty and urban delights Scandinavia has
                    to offer. In the first guidebook dedicated to the area, Bradt's West Sweden reveals the staggering variety of the area's experiences, includes a section on the region's history and culture and offers detailed maps of both the coastline
                    and the cities.</p>
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <p>
                        <a class="btn btn-default" href="http://www.amazon.com/West-Sweden-Including-Gothenburg-Regional/dp/1841625590" target="_blank">Buy book</a> 
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="visit-sweden-footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 visit-sweden-logo-top visit-sweden-white-border">
            <div class="row">
              <div class="col-xs-6 col-md-6">
                <div class="visit-sweden-logo-link visit-sweden-logo-left visit-sweden-logo-white-background">
                  <a href="index.php">
                    <img src="images/Visit_Sweden_logotype.svg" alt="" />
                  </a> 
                </div>
              </div>
              <div class="col-xs-6 col-md-6">
                <ul class="nav-pills visit-sweden-social-like-block">
                  <li>
                    <div class="like">
                      <div class="fb-like" data-href="https://www.facebook.com/swedentravel" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                      <br />
                      <div class="fb-share-button" data-layout="button"></div>
                    </div>
                    <a href="https://www.facebook.com/swedentravel" class="popup"> <span class="icons icon-t3-facebook"></span>  </a> 
                  </li>
                  <li>
                    <div class="like">
                      <a href="https://twitter.com/visitswedenus" data-show-count="false" class="twitter-follow-button" data-show-screen-name="false">Follow</a> 
                      <br />
                      <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a> 
                    </div>
                    <a href="https://twitter.com/visitswedenus" class="popup"> <span class="icons icon-t3-twitter"></span>  </a> 
                  </li>
                </ul>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-12 footer-about">
                            <h4>Visit Sweden</h4>
                            <p>Visit Sweden is the official marketing organization for Sweden as a travel destination.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6 visit-sweden-list visit-sweden-white-arrow-list">
                        <h4>Quick Links</h4>
                        <?php include 'inc-footer-menu.php';?>
                        <!-- <ul>
                          <li> <a href="travellers.html">Travellers</a>  </li>
                          <li> <a href="contest.html">Sweepstakes</a>  </li>
                          <li> <a href="activities.html">Activities</a>  </li>
                        </ul> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 visit-sweden-list">
                    <h4>Sign up for our newsletter</h4>
                    <p>Get inspiration, offers and participate in competitions where you can win experiences for life. Sign up for our newsletter by typing in your email address below.</p>
                    <div class="visit-sweden-subscription">
                      <form action="http://visitsweden.us7.list-manage.com/subscribe/post?u=c403aaad1aeaccc1c2d8be308&id=fc5639cff2" method="post" id="mc-embedded-subscribe-form-footer" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="" _lpchecked="1">
                        <div class="form-group">
                          <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Your e-mail   " required="">
                        </div>
                        <div style="position: absolute; left: -5000px">
                          <input type="text" name="b_c403aaad1aeaccc1c2d8be308_fc5639cff2" tabindex="-1" value="">
                        </div>
                        <div class="subscribe-button">
                          <input type="submit" value="Yes please" name="subscribe" class="button">
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-12 footer-copyright-description">
                    <div class="row">
                      <div class="col-md-12">
                        <p>
                          <b>COPYRIGHT 2015 VISITSWEDEN
                            <br />ALL RIGHTS RESERVED.
                            <a href="http://www.visitsweden.com/" target="_blank">VISITSWEDEN.COM</a> 
                          </b>
                        </p>
                      </div>
                      <div class="col-md-6">
                        <p>
                          <b>ADMINISTERED BY VISITSWEDEN.</b>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 visit-sweden-sender-model visit-sweden-sender-model-white">
            <div class="visit-sweden-logo-link">
              <a href="http://sweden.se" target="_blank">
                <img src="images/sweden-logo.svg" alt="">
              </a> 
            </div>
            <div class="visit-sweden-logo-link">
              <a href="http://www.visitsweden.com/" target="_blank">
                <img src="images/visit-logo.svg" alt="">
              </a> 
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

      <!-- Background of PhotoSwipe.
         It's a separate element as animating opacity is faster than rgba(). -->
      <div class="pswp__bg"></div>

      <!-- Slides wrapper with overflow:hidden. -->
      <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">

            <!--  Controls are self-explanatory. Order can be changed. -->
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

            <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->

            <!-- element will get class pswp__preloader--active when preloader is running -->
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
          </button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
          </button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <script src="imagesloaded.pkgd.min.js"></script>
    <script src="main.js"></script>
    <script src="local.js"></script>
    <script type="text/javascript" src="//cdn.resultify.com/clients/visitsweden_new_york/"></script>

    <script>
      $(document).ready(function(){
        var activeMenu = $('.navbar-nav a[href^="activities"]:first')
        $(activeMenu).parent().addClass('active');
      });
    </script>

  </body>
</html>
