<div class="section dark hidden-xs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">It's closer than you think</h3>
        <h2 class="text-center">Summer in Sweden</h2>
      </div>
    </div>
  </div>
  <ul class="visit-sweden-inspiration-feed">
    <li class="slice child-1">
      <a href="http://www.volvomuseum.com/" class="backgroundImage" style="background-image:url(images/designmom-Volvo-Tour3.jpg)" target="_blank"></a> 
      <div class="content">
        <h3 class="insta-user">Volvo Museum</h3>
        <p class="insta-text">A world of iconic car models, groundbreaking innovations and imaginative prototypes.</p>
        <a href="http://www.volvomuseum.com/" class="read-more" target="_blank">See full image</a> 
      </div>
    </li>
    <li class="slice child-2">
      <a class="backgroundImage" href="http://www.vastsverige.com/en/products/52836/Lake-Hornborga/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n&utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n" style="background-image:url(images/Lake-Hornborga_7.jpg)"
      target="_blank"></a> 
      <div class="content">
        <h3 class="insta-user">Lake Hornborga</h3>
        <p class="insta-text">This is where the dancing cranes are. Great spot to hike.</p>
        <a href="http://www.vastsverige.com/en/products/52836/Lake-Hornborga/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n&utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Ecotourist_Hornborgasj%C3%B6n" class="read-more"
        target="_blank">See full image</a> 
      </div>
    </li>
    <li class="slice child-3">
      <a class="backgroundImage" href="http://www.goteborg.com/en/foodtrucks/" style="background-image:url(images/food-stall.jpg)" target="_blank"></a> 
      <div class="content">
        <h3 class="insta-user">Food stalls</h3>
        <p class="insta-text">Try the food stalls in Magasinsgatan, Gothenburg.</p>
        <a href="http://www.goteborg.com/en/foodtrucks/" class="read-more" target="_blank">See full image</a> 
      </div>
    </li>
    <li class="slice child-4">
      <a class="backgroundImage" href="http://www.vastsverige.com/en/products/47168/Lacko-Slott-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_L%C3%A4ck%C3%B6%20slott" style="background-image:url(images/Lacko-Castle-Kallandso_5.jpg)" target="_blank"></a> 
      <div class="content">
        <h3 class="insta-user">Läckö Castle</h3>
        <p class="insta-text">Läckö Castle is best known as Magnus de la Gardie´s magnificent castle on the shores of Lake Vänern, but it is much older by far</p>
        <a href="http://www.vastsverige.com/en/products/47168/Lacko-Slott-Kallandso/?utm_source=Banner&utm_medium=Banner&utm_campaign=volvous_Foodie_L%C3%A4ck%C3%B6%20slott" class="read-more" target="_blank">See full image</a> 
      </div>
    </li>
    <li class="slice child-5">
      <a class="backgroundImage" href="http://www.goteborg.com/en/universeum/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" style="background-image:url(images/universum.jpg)" target="_blank"></a> 
      <div class="content">
        <h3 class="insta-user">Universeum</h3>
        <p class="insta-text">Universeum is located in the heart of Gothenburg and is Scandinavia's largest science centre.</p>
        <a href="http://www.goteborg.com/en/universeum/?utm_source=visitsweden&utm_medium=contentad&utm_campaign=inavolvo" class="read-more" target="_blank">See full image</a> 
      </div>
    </li>
  </ul>
</div>

<script>
  function assignInstagramFeed(jsonData){
    
    //console.log(jsonData[0]);
    //var json = $.parseJSON(jsonData);
    if(jsonData.length >= 5){
      for(i=0;i<5;i++){
        $('.visit-sweden-inspiration-feed li:nth-child('+(i+1)+') .backgroundImage').css('background-image','url(' + jsonData[i].src + ')');
        $('.visit-sweden-inspiration-feed li:nth-child('+(i+1)+') .insta-user').text(jsonData[i].user);
        $('.visit-sweden-inspiration-feed li:nth-child('+(i+1)+') .insta-text').text(jsonData[i].title + '...');

        $('.visit-sweden-inspiration-feed li:nth-child('+(i+1)+') .read-more').attr('href',jsonData[i].link);
        $('.visit-sweden-inspiration-feed li:nth-child('+(i+1)+') .backgroundImage').attr('href',jsonData[i].link);
      }
    }
  }
</script>



