<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/blogger-expertvagabond.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Matthew, the explorer</h4>
    <p class="bodytext">Matthew Karsten is the Expert Vagabond. For the past 4 years he’s been on a mission to inspire others with entertaining stories...</p>
    <a class="btn btn-default" href="the-explorer-matthew.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/blogger-ordinarytraveler.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Christy & Scott, the adventurers</h4>
    <p class="bodytext">Ordinary Traveler is one of the leading adventure travel and luxury blogs -- founded by two pro photographers with...</p>
    <a class="btn btn-default" href="the-adventurer-christy.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/blogger-gabby.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Gaby, the foodie</h4>
    <p class="bodytext">Gaby Dalkin is a cookbook author, private chef and food/lifestyle writer based in Los Angeles.</p>
    <a class="btn btn-default" href="the-foodie-gaby.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/curator.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Gabrielle Blair, The Curator</h4>
    <p class="bodytext">Mother of six and lives in Oakland, CA with her hubby Benjamin.</p>
    <a class="btn btn-default" href="the-curator.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/ecos3.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Bret & Mary, The Eco-Tourists</h4>
    <p class="bodytext">Bret & Mary launched Green Global Travel in 2010 to pursue their passion for ecotourism.</p>
    <a class="btn btn-default" href="the-eco-tourist.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/Explorer.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Jennifer and Tim, The Explorers</h4>
    <p class="bodytext">They love adventure and wine and often try to combine the two</p>
    <a class="btn btn-default" href="the-explorer.php">Read more</a> 
  </div>
</div>

<div class="visit-sweden-ny-blogger image-center">
  <div class="image-wrap">
    <div class="image-center-outer">
      <div class="image-center-inner">
        <figure class="image">
          <img src="images/foodies.jpg" alt="">
        </figure>
      </div>
    </div>
  </div>
  <div class="text">
    <h4>Amanda G. Bottoms, The Foodie</h4>
    <p class="bodytext">Traveling with her good friend Stephanie they are eager to try all the Swedish delights.</p>
    <a class="btn btn-default" href="the-foodie.php">Read more</a> 
  </div>
</div>